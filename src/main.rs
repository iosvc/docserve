use actix_files as fs;
use actix_web::{App, HttpServer};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new().service(
            fs::Files::new("/", ".")
                .show_files_listing()
                .redirect_to_slash_directory()
                .index_file("index.html"),
        )
    })
    .bind(("127.0.0.1", 3000))?
    .run()
    .await
}

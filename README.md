# docserve
Non configurable static web server.

## What
Serve static html files from the current directory locally on port 3000 with millisecond response times as provided by actix-web.

## Why
Other off the shelf one liner web server either did not provide the correct behavior for cargo-doc relative paths, or had unsatisfactory performance.

## Typical workflow

```sh
cd mycrate
cargo doc
cd target/doc
docserve &
xdg-open http://localhost:3000/mycrate
```

